<?php

namespace App\SummeryOfOrganization;

use App\Database as DB;

use PDO;
use App\Message\Message;
use App\Utility\Utility;
class SummeryOfOrganization extends DB
{

    public $id = "";

    public $name = "";

    public $summery = "";


    public function __construct()
    {

        parent::__construct();

    }
    public function index($Mode="ASSOC"){

        $STH = $this->conn->query('SELECT * FROM `summary_of_organization`');


        if($Mode=="OBJ")   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;


    }


    public function setData($data = NULL){
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name = $data['name'];
        }
        if(array_key_exists('summery',$data)){
            $this->summery = $data['summery'];
        }
    }

    public function store(){
        $DBH = $this->conn;
        $data = array($this->name,$this->summery);
        $STH = $DBH->prepare("INSERT INTO `summary_of_organization`(`id`, `name`, `summery`) VALUES (NULL ,?,?)");
        $STH->execute($data);
        Message::message("<div id='msg'></div><h3 align='center'>[ Name of the Organization: $this->name ] , [ Summery of the organization: $this->summery ] <br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');


    }


}