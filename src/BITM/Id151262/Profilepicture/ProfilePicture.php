<?php

namespace App\ProfilePicture;

use App\Database as DB;

use PDO;
use App\Message\Message;
use App\Utility\Utility;
class ProfilePicture extends DB
{
    public $id;
    public $name;
    public $picture;

    public function __construct()
    {

        parent::__construct();

    }
    public function index($Mode="ASSOC"){

        $STH = $this->conn->query('SELECT * FROM `profile_picture`');


        if($Mode=="OBJ")   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;


    }

    public function setData($data = NULL){
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name = $data['name'];
        }
        if(array_key_exists('image',$data)){
            $this->picture = $data['image'];
        }
    }

    public function store(){
        $DBH = $this->conn;
        $data = array($this->name,$this->picture);
        $STH = $DBH->prepare("INSERT INTO `profile_picture`(`id`, `username`, `pro_pic`) VALUES (NULL ,?,?)");
        $STH->execute($data);
        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ Picture: $this->picture] <br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');


    }


}// end of BookTitle class