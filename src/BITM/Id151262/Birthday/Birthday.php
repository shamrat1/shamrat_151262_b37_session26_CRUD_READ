<?php

namespace App\Birthday;

use App\Database as DB ;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class Birthday extends DB
{
    public $id;
    public $name;
    public $date;

    public function __construct()
    {

        parent::__construct();

    }
    public function index($Mode="ASSOC"){

        $STH = $this->conn->query('SELECT * FROM `birthday`');


        if($Mode=="OBJ")   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;


    }



    public function setData($data = NULL){
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name = $data['name'];
        }
        if(array_key_exists('date',$data)){
            $this->date = $data['date'];
        }
    }

    public function store(){
        $DBH = $this->conn;
        $data = array($this->name,$this->date);

        $STH = $DBH->prepare("INSERT INTO `birthday` (`id`, `username`, `birthday`) VALUES (NULL,?,?)");
        $STH->execute($data);

        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ Birthday: $this->date ] <br> Data Has Been Inserted Successfully!</h3></div>");
        Utility::redirect('create.php');


    }


}// end of BookTitle class