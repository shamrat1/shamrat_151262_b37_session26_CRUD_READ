<?php

namespace App\BookTitle;

use App\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

use PDO;


class BookTitle extends DB
{

    public $id = "";

    public $book_title = "";

    public $author_name = "";


    public function __construct()
    {

        parent::__construct();

    }

    public function setData($data = NULL){
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('book_title',$data)){
            $this->book_title = $data['book_title'];
        }
        if(array_key_exists('author_name',$data)){
            $this->author_name = $data['author_name'];
        }
    }

    public function view(){
        $STH = $this->conn->query('SELECT * from book_title WHERE `id`='.$this->id);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $objAllData = $STH->fetchAll();
        return $objAllData;
    }

    public function update(){

        $data = array($this->book_title,$this->author_name);
        $STH = $this->conn->prepare("UPDATE `book_title` SET `book_title` = ".$this->book_title." `author_name` =".$this->author_name." WHERE `id` =".$this->id);
        $STH->execute();

        Message::message("<div id='msg'></div><h3 align='center'>[ BookTitle: $this->book_title ] , [ AuthorName: $this->author_name ] <br> Data Has Been Updated Successfully!</h3></div>");


        Utility::redirect('index.php');
    }


    public function store(){
        $DBH = $this->conn;
        $data = array($this->book_title,$this->author_name);
        $STH = $DBH->prepare("INSERT INTO `book_title`(`id`, `book_title`, `author_name`) VALUES (NULL ,?,?)");
        $STH->execute($data);

        Message::message("<div id='msg'></div><h3 align='center'>[ BookTitle: $this->book_title ] , [ AuthorName: $this->author_name ] <br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');

        
    }
    public function index($Mode="ASSOC"){

        $STH = $this->conn->query('SELECT * from book_title');


        if($Mode=="OBJ")   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;


    }





}