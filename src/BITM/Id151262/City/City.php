<?php

namespace App\City;

use App\Database as DB;
use PDO;
use App\Message\Message;
use App\Utility\Utility;
class City extends DB
{

    public $id = "";

    public $name = "";

    public $City = "";


    public function __construct()
    {

        parent::__construct();

    }
    public function index($Mode="ASSOC"){

        $STH = $this->conn->query('SELECT * FROM `city`');


        if($Mode=="OBJ")   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;


    }

    public function setData($data = NULL){
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('username',$data)){
            $this->name = $data['username'];
        }
        if(array_key_exists('city',$data)){
            $this->City = $data['city'];
        }
    }

    public function store(){
        $DBH = $this->conn;
        $data = array($this->name,$this->City);
        $STH = $DBH->prepare("INSERT INTO `city`(`id`, `username`, `city_name`) VALUES (NULL ,?,?)");
        $STH->execute($data);

        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ City: $this->City ] <br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');


    }


}